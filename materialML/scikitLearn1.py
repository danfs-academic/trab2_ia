# scikit-learn: 
# https://scikit-learn.org/stable/user_guide.html

# 1.Primeira aula:
# http://scikit-learn.org/stable/tutorial/statistical_inference/supervised_learning.html

# 1.1. load and shape of datasets (Carregar iris e ver suas propriedades)
import numpy as np
from sklearn import datasets
iris = datasets.load_iris()
iris_X = iris.data
iris_y = iris.target
print("UNICOS E FORMATOS")
print(np.unique(iris_y))
print(iris.data.shape, iris.target.shape)

# 1.2. Training and testing with knn
np.random.seed(0)
indices = np.random.permutation(len(iris_X))
iris_X_train = iris_X[indices[:-10]]
iris_y_train = iris_y[indices[:-10]]
iris_X_test = iris_X[indices[-10:]]
iris_y_test = iris_y[indices[-10:]]

from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier()
knn.fit(iris_X_train, iris_y_train)
print("PREVISÃO E REAL")
print(knn.predict(iris_X_test))
print(iris_y_test)

# 1.3. Divisão percentual 60/40
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.4, random_state=0)
print("FORMATOS DOS TREINOS E DO TESTES")
print(X_train.shape, y_train.shape)
print(X_test.shape, y_test.shape)
knn = KNeighborsClassifier()
knn.fit(X_train, y_train) 
print("RESULTADOS 60/40")
print(knn.predict(X_test))
print(y_test)
print(knn.score(X_test, y_test))
