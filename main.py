# Dict das bases e seus valores correspondentes
from sklearn import datasets
bases = {
    'iris'         : datasets.load_iris,
    'digits'       : datasets.load_digits,
    'wine'         : datasets.load_wine,
    'breast_cancer': datasets.load_breast_cancer
}

# Import dos classificadores pré-existentes
from sklearn.naive_bayes    import GaussianNB             as nbGauss
from sklearn.neighbors      import KNeighborsClassifier   as knnClass
from sklearn.tree           import DecisionTreeClassifier as treeClass
from sklearn.neural_network import MLPClassifier          as neuralClass
from sklearn.ensemble       import RandomForestClassifier as forestClass

# Import dos classificadores implementados
from zeroR         import ZeroRClassifier         as zeroR
from oneR          import OneRClassifier          as oneR
# from oneRProb      import OneRProbClassifier      as oneRProb
from centroide     import CentroideClassifier     as centroide
# from centroideOneR import CentroideOneRClassifier as zeroR

# Dict dos classificadores e seus construtures correspondentes
classifiers = {
    'zeroR'            : zeroR,
    'oneR'             : oneR,
    # 'oneRProb'         : oneRProb,
    'centroide'        : centroide,
    # 'centroideOneR'    : centroideOneR,
    'naive_bayes_gauss': nbGauss,
    'knn'              : knnClass,
    'tree'             : treeClass,
    'neural'           : neuralClass,
    'forest'           : forestClass
}

# Lista de quais não usam hiperparametros
sem_hiper = [
    'zeroR',
    'oneR',
    # 'oneRProb',
    'centroide',
    # 'centroideOneR',
    'naive_bayes_gauss'
]

# Lista de quais usam hiperparametros
com_hiper = [
    'knn',
    'tree',
    'neural',
    'forest'
]

# Dict dos parâmetros a serem usados nos clasificadores necessários
params = {
    'knn'    : {'n_neighbors' : [1, 3, 5, 7, 10]},
    'tree'   : {'max_depth'   : [None, 3, 5, 10]},
    'neural' : {'max_iter'    : [50, 100, 200],
                'hidden_layer_sizes' : (15,),
                },
    'forest' : {'n_estimators': [10, 20, 50, 100]} 
}

# Outros imports necessários
import numpy as np
from sklearn.model_selection import KFold, cross_val_score, GridSearchCV
from typing                  import List
# Ignorar os warnings de mudança de padrões de variáveis default das novas versões do sklearn
import warnings 
warnings.filterwarnings('ignore')
warnings.warn

# Gera o boxplot baseado nos 'scores' de uma 'base'
# 'hiperpar' serve para identificar e diferenciar os dois boxplots gerados
def gera_boxplot(base, scores, hiperpar):
    # Evita tentar gerar um boxplot vazio
    if scores == []: return
    
    # Faz os imports apenas se essa função for chamada, pois são imports demorados
    from matplotlib.pyplot import figure
    from seaborn           import boxplot
    # Divisão dos nomes do classificador e dos valores
    data    = [i[1] for i in scores]
    classis = [i[0] for i in scores]
    # Geração do boxplot
    fig = figure()
    graph = boxplot(data=data, showmeans=True, meanprops={'markerfacecolor': 'white', 'markeredgecolor': 'black'})
    graph.set_xticklabels(classis)
    graph.set(xlabel='classificadores', ylabel='acurácia')
    # Geração do arquivo para armazenar o boxplot
    fig.savefig(f'boxplots/{base}_{hiperpar}Hiperparam.svg', dpi=fig.dpi)

# Gera uma tabela em formato LaTeX baseado em 'tabela'
# A tabela é gerada num arquivo 'base'.txt, na pasta tabelas/
def gera_tabela(base, tabela):
    from pandas import DataFrame
    data = DataFrame(tabela)
    arq = open('tabelas/'+base+'.txt', 'w')
    print(data.to_latex(index=False), file=arq)
    arq.close()

# Executa a validação de 10 folds do classificador usando a base.
# Retorna uma lista das precisões alcançadas pelo classificador.
# Feito manualmente (sem cross_val_score) por motivos de aprendizado e variedade
def hiperless(base, classConstruc) -> np.array:
    # Inicializações das variáveis utilizadas
    score  = []
    classi = classConstruc()
    base_y = base.target
    base_X = base.data
    folds  = KFold(n_splits=10, shuffle=True, random_state=1)
    # Extração dos índices para cada fold e execução do treinamento e teste
    for train_index, teste_index in folds.split(base_X):
        X_train = base_X[train_index]
        y_train = base_y[train_index]
        X_teste = base_X[teste_index]
        y_teste = base_y[teste_index]
        classi.fit(X_train, y_train)
        score.append(classi.score(X_teste, y_teste))
    # Retorna a lista de resultados
    return np.array(score)

# Recebe o nome do classificador (para poder acessar o dict de parâmetros) e a base a ser usada.
# Retorna uma lista das precisões alcançadas pelo classificador.
def hiper(base, className) -> np.array:
    classi = classifiers[className]()
    gs = GridSearchCV(classi, param_grid=params[className], scoring='accuracy', cv = 4)
    return cross_val_score(gs, base.data, base.target, scoring='accuracy', cv=10)

# Execução principal do código
if __name__ == "__main__":
    for nomeBase, construcBase in bases.items():
        base = construcBase()
        tabela = {'Nome': [], 'Media': [], 'StdDev': []}
        # Faz a análise dos classificadores sem hiperparametro, gerando um boxplot e uma tabela
        scores = []
        for nomeClass in sem_hiper:
            result  = hiperless(base, classifiers[nomeClass])
            scores.append((nomeClass, result))
            tabela['Nome'].append(nomeClass)
            tabela['Media'].append(result.mean())
            tabela['StdDev'].append(result.std())
        gera_boxplot(nomeBase, scores, 'Sem')

        # Repete o processo acima, mas com os classificadores com hiperparametros
        scores = []
        for nomeClass in com_hiper:
            result = hiper(base, nomeClass)
            scores.append((nomeClass, result))
            tabela['Nome'].append(nomeClass)
            tabela['Media'].append(result.mean())
            tabela['StdDev'].append(result.std())
        gera_boxplot(nomeBase, scores, 'Com')
        # Gera a tabela com todos os resultados da base
        gera_tabela(nomeBase, tabela)