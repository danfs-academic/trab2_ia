from sklearn.base             import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels

def _most_common(list):
    from numpy import unique, argmax
    # Extrai todos os valores únicos, e suas quantidades
    values, count = unique(list, return_counts=True)
    # Identifica aonde está a maior contagem, e retorna o valor nessa posição
    index = argmax(count)
    return values[index]

class ZeroRClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, demo_param='demo'):
        self.demo_param = demo_param

    def fit(self, X, y):
        X, y = check_X_y(X, y)           # Check that X and y have correct shape
        self.classes_ = unique_labels(y) # Store the classes seen during fit
        self.X_ = X
        self.y_ = y
        self.maioria_ = _most_common(y)
        # Return the classifier
        return self

    def predict(self, X):
        X = check_array(X) # Input validation
        return [self.maioria_]*len(X)

if __name__ == "__main__":
    from sklearn import datasets
    iris = datasets.load_iris()
    
    from sklearn.model_selection import train_test_split, cross_val_score
    X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.4, random_state=0)
    zr = ZeroRClassifier()
    zr.fit(X_train, y_train) 
    print(zr.predict(X_test))
    print(zr.score(X_test, y_test))
    scores = cross_val_score(zr, iris.data, iris.target, cv=5)
    print(scores)