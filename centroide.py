from sklearn.base             import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels
from scipy.spatial.distance   import euclidean
from numpy                    import add

def _centroides(classes, X, y):
    centros = {} # Dict dos centroides de cada classe
    for i in classes:
        soma = [0]*len(X[0]) # Cria a soma das coordenadas
        qtdd = 0             # Qtdd de nós que compõe o centróide
        for j, classe in enumerate(y):
            if classe == i:
                soma  = add(soma, X[j])
                qtdd += 1

        centros[i] = [i/qtdd for i in soma]
    return centros

class CentroideClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, demo_param='demo'):
        self.demo_param = demo_param

    def fit(self, X, y):
        X, y = check_X_y(X, y)           # Check that X and y have correct shape
        self.classes_ = unique_labels(y) # Store the classes seen during fit
        self.X_ = X
        self.y_ = y
        self.centros_ = _centroides(self.classes_, X, y)
        # Return the classifier
        return self

    def predict(self, X):
        X   = check_array(X) # Input validation
        ret = []
        # Varrer todos os elementos
        for elem in X:
            # Lista das distâncias do elemento para os centros das classes
            dists = [(classe, euclidean(elem, centro))
                    for classe, centro in self.centros_.items()]
            # Encontrar a menor distância e gravar a classe
            closest = min(dists, key=lambda x: x[1])
            ret.append(closest[0])

        return ret

if __name__ == "__main__":
    from sklearn import datasets
    iris = datasets.load_iris()
    
    from sklearn.model_selection import train_test_split, cross_val_score
    X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.4, random_state=0)
    cc = CentroideClassifier()
    cc.fit(X_train, y_train)
    print(cc.predict(X_test))
    print(cc.score(X_test, y_test))
    scores = cross_val_score(cc, iris.data, iris.target, cv=5)
    print(scores)