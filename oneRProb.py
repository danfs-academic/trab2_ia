from sklearn.base             import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels

def _most_common(list):
    from numpy import unique, argmax
    # Extrai todos os valores únicos, e suas quantidades
    values, count = unique(list, return_counts=True)
    # Identifica aonde está a maior contagem, e retorna o valor nessa posição
    index = argmax(count)
    return values[index]

# Faz a escolha de qual caracteristica utilizar, e qual o intervalo
def _choose_class(classes, X, y_true):
    from numpy import array, Inf
    from sklearn.metrics import confusion_matrix
    max_erro = Inf
    nint     = len(classes) # Número de intervalos
    samples  = len(X)       # Número de amostras
    # Lista dos índices que iniciam cada intervalo
    indexes = [int(n*samples/nint) for n in range(nint+1)]
    # Realizar o loop para identificar a melhor caracteristica
    for i in range(len(X[0])):
        valores = array([caso[i] for caso in X]) # Lista dos valores da característica 'i'
        order   = valores.argsort()              # Lista dos índices que ordenam 'valores'
        slices  = []                             # Lista dos índices de intervalos
        for j in range(len(indexes)-1):
            slices.append(order[indexes[j]:indexes[j+1]])
        # Faz a análise para cada intervalo
        erros   = 0
        interv  = []
        classes = []
        for j in range(len(slices)):
            slice_valor = valores[slices[j]]
            slice_class = y_true[slices[j]]
            maioria     = _most_common(slice_class)
            limit       = slice_valor[-1]   # Limite superior do intervalo
            erros      += len([i for i in slice_class if i != maioria])
            interv.append(limit)
            classes.append(maioria)
        # Checa se deve trocar a classe
        if erros < max_erro:
            max_erro     = erros
            final_caract = i
            final_interv = interv
            final_class  = classes
    
    return final_caract, final_interv, final_class


class OneRProbClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, demo_param='demo'):
        self.demo_param = demo_param

    def fit(self, X, y):
        X, y = check_X_y(X, y)           # Check that X and y have correct shape
        self.classes_ = unique_labels(y) # Store the classes seen during fit
        self.X_ = X
        self.y_ = y
        self.class_, self.interval_, self.classes_oneR = _choose_class(self.classes_, X, y)

        # Return the classifier
        return self

    def predict(self, X):
        X = check_array(X) # Input validation
        ret = []
        # Para cada amostra
        for i in X:
            flag = 0
            valor = i[self.class_] # Valor da característica a ser analizado
            for n, j in enumerate(self.interval_):
                # Se o valor estiver dentro do intervalo, adicionar a classe do intervalo
                if valor < j:
                    ret.append(self.classes_oneR[n])
                    flag = 1
                    break
            # Se não estiver em nenhum intervalo, usar o maior intervalo como referencia
            if flag == 0: ret.append(self.classes_oneR[-1])

        return ret

if __name__ == "__main__":
    from sklearn import datasets
    iris = datasets.load_iris()
    
    from sklearn.model_selection import train_test_split, cross_val_score
    X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.4, random_state=0)
    oRP = OneRProbClassifier()
    oRP.fit(X_train, y_train) 
    print(oRP.predict(X_test))
    print(oRP.score(X_test, y_test))
    scores = cross_val_score(oRP, iris.data, iris.target, cv=5)
    print(scores)